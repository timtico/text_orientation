import os
import glob
import cv2
import numpy
from PIL import Image
from matplotlib import pyplot as plt


class Orientation:
    ''' orientates an image in portrait orientation if it is
    found to be in landscape '''

    def load_image(filename):
        ''' load jpg image from file'''
        return Image.open(filename).convert('RGB')


    def img_to_color_array(image):
        ''' convert a pil image to a color space image array suited to be
        used with opencv, note the conversion from RGB to BGR '''
        return cv2.cvtColor(numpy.array(image), cv2.COLOR_RGB2BGR)


    def img_to_gray_array(image):
        '''convert a pil image to a gray space image'''
        return cv2.cvtColor(numpy.array(image), cv2.COLOR_RGB2GRAY)


    def blur(image_array):
        ''' blurs the image '''
        return cv2.blur(image_array,(20,20))


    def threshold(image_array):
        ''' convert image to black/white, automatic threshold detection'''
        _, img_array_bin = cv2.threshold(image_array, 128, 255, cv2.THRESH_BINARY | cv2.THRESH_OTSU)
        return img_array_bin


    def detect_edges(image_array, threshold1 = 100, threshold2 = 100):
        ''' performs canny edge detection on the image_array
        returns. Note that there is another setting: aperturesize '''
        return cv2.Canny(image_array, threshold1, threshold2, 3)


    def hough_transform_p(edges, min_line_len, max_line_gap):
        ''' performs a hough transformation on edges
        the result is a array of line points (x1,y1,x2,y2)'''
        return cv2.HoughLinesP(edges,1,numpy.pi/180,100,min_line_len, max_line_gap)


    def calculate_deltas(line_arrays):
        ''' takes a list of arrays that contain line coordinates
        calculates the delta x and delta y'''
        array_stack = numpy.vstack(line_arrays)
        deltax = numpy.mean(abs(array_stack[:,0] - array_stack[:,2]))
        deltay = numpy.mean(abs(array_stack[:,1] - array_stack[:,3]))
        return deltax,deltay


    def rotate_to_portrait(image):
        ''' rotates an Image into portrait mode'''
        return image.rotate(-90, expand=True) 
    

    @classmethod 
    def get_lines(cls, img):
        ''' detect lines on a blurred and thresholded image
        using a hough transformation
        in: Image object (PIL)
        out: array with line coordinates'''
        img_array_binary = cls.threshold(cls.blur(cls.img_to_gray_array(img)))
        edges = cls.detect_edges(img_array_binary, 100, 200)
        return cv2.HoughLinesP(edges,1,numpy.pi/180,100,150,20)

    
    @classmethod
    def is_landscape(cls, image):
        ''' takes a Image object and determines if the text in the image
        is orientated in landscape or not '''
        delta_x,delta_y = cls.calculate_deltas(cls.get_lines(image))
        if delta_y > delta_x:
            return True
        else:
            return False
        

    @classmethod
    def portrait_orientation(cls, image):
        ''' rotates an image into portrait mode, if image
        is in landscape mode '''
        if cls.is_landscape(image):
            return cls.rotate_to_portrait(image)
        else:
            return image
        

    @classmethod
    def test(cls):
        ''' tests the orientation of a set of files with known orientation '''
        current_dir = os.path.dirname(os.path.realpath(__file__))
        test_images = sorted(glob.glob(os.path.join(current_dir, 'test_images') + '/*.jpg'))
        assert [cls.is_landscape(cls.load_image(f)) for f in test_images] == [False, True, True, False]

if __name__ == '__main__':
    Orientation.test()
    
